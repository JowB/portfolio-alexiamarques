<?php


namespace App\Controller;


use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/projects")
 * Class ProjectController
 * @package App\Controller
 */
class ProjectController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/categories/{id}", name="project_by_category", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function getProjectByCategory($id)
    {
        $projects = $this->getDoctrine()->getRepository(Project::class)->findBy(
            ['category' => $id]
        );

        $projectsJson = $this->serializer->serialize($projects, 'json');

        return new Response($projectsJson);
    }
}
