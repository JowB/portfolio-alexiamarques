export class Project {
    id: number;
    title: string;
    tools: string;
    pictures: string;
    link: string;
    category: string;
    description: string;
}
