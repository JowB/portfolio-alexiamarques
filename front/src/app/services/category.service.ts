import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../models/category';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    constructor(private http: HttpClient) { }

    /**
     * Récupère une catégorie
     */
    getCategory(url: string): Observable<Category> {
        return this.http.get<Category>('http://localhost:8000' + url);
    }

    /**
     * Récupère toutes les catégories
     */
    getAllCategories(): Observable<Category[]> {
        return this.http.get<Category[]>('http://localhost:8000/api/categories');
    }
}
