import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Project} from '../models/project';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    constructor(private http: HttpClient) { }

    /**
     * Récupère la liste des projets
     */
    getProjects(): Observable<Project[]> {
        return this.http.get<Project[]>('http://localhost:8000/api/projects');
    }

    /**
     * Récupère la liste des projets en fonction de la catégorie
     */
    getProjectsByCategory(categoryId: number): Observable<Project[]> {
        return this.http.get<Project[]>('http://localhost:8000/projects/categories/' + categoryId);
    }
}
