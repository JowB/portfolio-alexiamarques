import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CategoryService} from '../services/category.service';
import {Category} from '../models/category';
import {Project} from '../models/project';
import {ProjectService} from '../services/project.service';

@Component({
    selector: 'app-project-filters',
    templateUrl: './project-filters.component.html',
    styleUrls: ['./project-filters.component.css']
})
export class ProjectFiltersComponent implements OnInit {

    categories: Category[];
    @Output() projectsChanged: EventEmitter<Project[]> = new EventEmitter();

    constructor(private categoryService: CategoryService,
                private projectService: ProjectService) { }

    ngOnInit() {
        this.getCategories();
    }

    getCategories() {
        this.categoryService.getAllCategories()
            .subscribe((categories: Category[]) => {
                this.categories = categories;
            }, err => {
                console.log(err);
            });
    }

    filterProject(id?: number) {
        if (id) {
            this.projectService.getProjectsByCategory(id)
                .subscribe((projects: Project[]) => {
                    this.projectsChanged.emit(projects);
                }, err => {
                    console.log(err);
                });
        } else {
            this.projectService.getProjects()
                .subscribe((projects: Project[]) => {
                    this.projectsChanged.emit(projects);
                }, err => {
                    console.log(err);
                });
        }
    }
}
