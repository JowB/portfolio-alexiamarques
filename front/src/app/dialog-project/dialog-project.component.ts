import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CategoryService} from '../services/category.service';
import {Category} from '../models/category';
import {Project} from '../models/project';

@Component({
    selector: 'app-dialog-project',
    templateUrl: './dialog-project.component.html',
    styleUrls: ['./dialog-project.component.css']
})
export class DialogProjectComponent implements OnInit {

    category: Category;

    constructor(public dialogRef: MatDialogRef<DialogProjectComponent>,
                @Inject(MAT_DIALOG_DATA) public data: {project: Project},
                private categoryService: CategoryService) { }

    ngOnInit() {
        this.getCategory();
    }

    /**
     * Récupère la catégorie du projet
     */
    getCategory() {
        this.categoryService.getCategory(this.data.project.category)
            .subscribe((category: Category) => {
                this.category = category;
            }, err => {
                console.log(err);
            });
    }

    /**
     * Ouvre une page internet en direction du lien du projet
     */
    goToLink(url: string) {
        if (url !== null) {
            window.open(url, '_blank');
        }
    }
}
