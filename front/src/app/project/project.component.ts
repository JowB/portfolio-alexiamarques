import { Component, OnInit } from '@angular/core';
import {Project} from '../models/project';
import {ProjectService} from '../services/project.service';
import {MatDialog} from '@angular/material';
import {DialogProjectComponent} from '../dialog-project/dialog-project.component';

@Component({
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

    projects: Project[];

    constructor(private projectService: ProjectService, public dialog: MatDialog) {

    }

    ngOnInit() {
        this.getProjects();
    }

    /**
     * Récupère la liste des projets
     */
    getProjects() {
        this.projectService.getProjects()
            .subscribe((projects: Project[]) => {
                this.projects = projects;
            }, err => {
                console.log(err);
            });
    }

    /**
     * Ouvre une pop-up pour afficher les infos du projet
     */
    openDialog(project: Project) {
        this.dialog.open(DialogProjectComponent, {
            width: '80vw',
            data: {project: project}
        });
    }

    /**
     * Met à jour la liste des projets en fonction de la catégorie
     */
    projectsChangedHandler($event: Project[]) {
        this.projects = $event;
    }
}
