import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenuComponent} from './menu/menu.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectComponent} from './project/project.component';
import {ContactComponent} from './contact/contact.component';
import {AboutComponent} from './about/about.component';
import {ProjectFiltersComponent} from './project-filters/project-filters.component';
import {WebsiteComponent} from './website/website.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HomeComponent} from './home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatDialogModule, MatInputModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import { DialogProjectComponent } from './dialog-project/dialog-project.component';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        ProjectListComponent,
        ProjectComponent,
        ContactComponent,
        AboutComponent,
        ProjectFiltersComponent,
        WebsiteComponent,
        PageNotFoundComponent,
        HomeComponent,
        DialogProjectComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatButtonModule,
        HttpClientModule,
        MatDialogModule
    ],
    entryComponents: [
        DialogProjectComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
